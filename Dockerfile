FROM node:12.18.1 As build

WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install

COPY . .
RUN npm run build --prod

FROM nginx
COPY --from=build /usr/src/app/dist/EmployeeManager/ /usr/share/nginx/html