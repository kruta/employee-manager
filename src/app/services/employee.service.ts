import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Employee } from '../models/employee';
import { throwError, Observable, forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Country } from '../models/country';
import { JobCategory } from '../models/jobCategory';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private apiUrl = 'http://localhost/employees/';
  public countries: Country[];
  public jobCategories: JobCategory[];

  constructor(private http: HttpClient) {
  }

  handleError(error: HttpErrorResponse) {
    console.log('error', error);
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = 'Error: ' + error.error.message;
    } else {
      // Server-side errors
      errorMessage = 'Error Code: ' + error.status + '\nMessage: ' + error.message;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(this.apiUrl + 'countries.php').pipe(catchError(this.handleError));
  }

  getJobCategories(): Observable<JobCategory[]> {
    return this.http.get<JobCategory[]>(this.apiUrl + 'jobs.php').pipe(catchError(this.handleError));
  }

  getEmployees(): Observable<[Employee[], Country[], JobCategory[]]> {
    const employees = this.http.get<Employee[]>(this.apiUrl + 'list.php').pipe(catchError(this.handleError));
    const countries = this.getCountries();
    const jobCategories = this.getJobCategories();
    return forkJoin([employees, countries, jobCategories]);
  }

  getEmployee(id: number): Observable<[Employee, Country[], JobCategory[]]> {
    const employe = this.http.get<Employee>(this.apiUrl + 'detail.php?id=' + id).pipe(catchError(this.handleError));
    const countries = this.getCountries();
    const jobCategories = this.getJobCategories();
    return forkJoin([employe, countries, jobCategories]);
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.apiUrl + 'update.php', employee).pipe(catchError(this.handleError));
  }

  deleteEmployee(id: number): Observable<Employee[]> {
    return this.http.delete<Employee[]>(this.apiUrl + 'delete.php?id=' + id).pipe(catchError(this.handleError));
  }

}
