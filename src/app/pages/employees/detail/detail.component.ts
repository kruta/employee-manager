import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { ActivatedRoute } from '@angular/router';
import { Country } from 'src/app/models/country';
import { JobCategory } from 'src/app/models/jobCategory';
import { Gender } from 'src/app/models/gender';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  id: number;
  employee: Employee;
  country: Country;
  jobCategory: JobCategory;
  Gender: typeof Gender = Gender;

  constructor(private employeeService: EmployeeService, private activatedRoute: ActivatedRoute) {
    if (this.activatedRoute.snapshot.params.id) {
      this.id = +this.activatedRoute.snapshot.params.id;
    }
  }

  ngOnInit(): void {
    this.getEmployee(this.id);
  }

  getEmployee(id): void {
    this.employeeService.getEmployee(id).subscribe((data: [Employee, Country[], JobCategory[]]) => {
      this.employee = data[0];
      this.country = data[1].find(c => c.id === this.employee.countryId);
      this.jobCategory = data[2].find(j => j.id === this.employee.jobCategoryId);
    },
    error => {
      console.log('chyba', error);
    });
  }

}
