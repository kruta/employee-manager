import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Country } from 'src/app/models/country';
import { JobCategory } from 'src/app/models/jobCategory';
import { Gender } from 'src/app/models/gender';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id: number;
  employee: Employee;
  country: Country;
  countries: Country[];
  jobCategory: JobCategory;
  jobCategories: JobCategory[];
  Gender: typeof Gender = Gender;

  constructor(private employeeService: EmployeeService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    if (this.activatedRoute.snapshot.params.id) {
      this.id = +this.activatedRoute.snapshot.params.id;
    }
  }

  ngOnInit(): void {
    this.getEmployee(this.id);
  }

  getEmployee(id): void {
    this.employeeService.getEmployee(id).subscribe((data: [Employee, Country[], JobCategory[]]) => {
      console.log(data);
      this.employee = data[0];
      this.countries = data[1];
      this.country = this.countries.find(c => c.id === this.employee.countryId);
      this.jobCategories = data[2];
      this.jobCategory = this.jobCategories.find(j => j.id === this.employee.jobCategoryId);
    },
    error => {
      console.log('chyba', error);
    });
  }


  onSubmit() {
    console.warn('form has been submitted', this.employee);
    this.employee = { ...this.employee, jobCategoryId: +this.employee.jobCategoryId, countryId: +this.employee.countryId };
    this.employeeService.updateEmployee(this.employee).subscribe((data: any) => {
      console.log(data);
      this.router.navigate(['/detail', this.id]);
    },
    error => {
      console.log('chyba', error);
    });
  }

}
