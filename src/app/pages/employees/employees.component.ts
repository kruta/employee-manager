import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { Country } from 'src/app/models/country';
import { JobCategory } from 'src/app/models/jobCategory';
import { Gender } from 'src/app/models/gender';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees: Employee[];
  Gender: typeof Gender = Gender;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeeService.getEmployees().subscribe((data: [Employee[], Country[], JobCategory[]]) => {
      console.log(data);
      this.employees = data[0];
    },
    error => {
      console.log('chyba', error);
    });
  }

  deleteEmployee(id: number) {
    const ans = confirm('Opravdu chcete zaměstnance odstranit?');
    if (ans) {
      this.employeeService.deleteEmployee(id).subscribe((data) => {
        this.getEmployees();
      });
    }
  }

}
