export interface JobCategory {
    id: number;
    title: string;
}
