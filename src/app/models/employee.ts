import { Gender } from './gender';
import { JobCategory } from './jobCategory';
import { Country } from './country';

export interface Employee {
    id: number;
    firstName: string;
    lastName: string;
    birthDate: Date;
    gender: Gender;
    jobCategoryId: number | JobCategory;
    email: string;
    phoneNumber: string;
    countryId: number | Country;
    joinedDate: Date;
    exitedDate: Date;
}
